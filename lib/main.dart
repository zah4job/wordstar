import 'package:flutter/material.dart';
import 'package:wordstar/pages/game_page.dart';
import 'package:wordstar/pages/introduce_page.dart';
import 'package:wordstar/pages/loby_page.dart';
import 'package:wordstar/pages/room_page.dart';
import 'package:wordstar/pages/rooms_page.dart';
import 'package:wordstar/pages/solo_game_over/solo_game_over_page.dart';
import 'package:wordstar/pages/solo_game_page.dart';
import 'package:wordstar/pages/winner_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // initialRoute: '/introduce',
      initialRoute: '/sologameover',
      routes: <String, WidgetBuilder>{
        '/introduce': (BuildContext context) => const IntroducePage(),
        '/rooms': (BuildContext context) => const RoomsPage(),
        // '/lobby': (BuildContext context) => const LobbyPage(
        //       owner: '?',
        //       gameId: '0',
        //     ),
        // '/game': (BuildContext context) => const GamePage(gameId: 73),
        // '/winner': (BuildContext context) =>
        //     const WinnerPage(winner: 'winne-r'),
        '/sologame': (BuildContext context) => const SoloGamePage(),
      },
      onGenerateRoute: (settings) {
        if (settings.name == '/room') {
          final args = settings.arguments as String;
          return MaterialPageRoute(builder: (context) {
            return RoomPage(roomId: args);
          });
        }

        if (settings.name == '/lobby') {
          // lobby params : owner, gameId
          final args = settings.arguments as Map<String, String>;
          return MaterialPageRoute(builder: (context) {
            String owner = args['owner'] as String;
            String game = args['gameId'] as String;
            String visitor = args['visitor'] as String;
            return LobbyPage(
              owner: owner,
              gameId: game,
              visitor: visitor,
            );
          });
        }

        if (settings.name == '/winner') {
          final args = settings.arguments as Map<String, String>;
          return MaterialPageRoute(builder: (context) {
            String winner = args['winner'] as String;
            return WinnerPage(winner: winner);
          });
        }

        if (settings.name == '/game') {
          final args = settings.arguments as Map<String, dynamic>;
          return MaterialPageRoute(builder: (context) {
            int game = args['gameId'] as int;
            return GamePage(
              gameId: game,
            );
          });
        }

        // мб перенести в routes
        if (settings.name == '/sologame') {
          return MaterialPageRoute(builder: (context) {
            return const SoloGamePage();
          });
        }

        if (settings.name == '/sologameover') {
          final args = settings.arguments != null
              ? settings.arguments as Map<String, dynamic>
              : {};
          return MaterialPageRoute(builder: (context) {
            int game = args.containsKey('gameId') ? args['gameId'] as int : 1;
            var playerWords = args.containsKey('playerWords')
                ? args['playerWords'] as List<String>
                : ['слово', 'другоеслово'];
            var botWords = args.containsKey('botWords')
                ? args['botWords'] as List<String>
                : [
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                    'игрок',
                    'сказал',
                  ];
            var win = args.containsKey('win') ? args['win'] as bool : true;
            return SoloGameOverPage(
              gameId: game,
              win: win,
              playerWords: playerWords,
              botWords: botWords,
            );
          });
        }

        return null;
      },
    );
  }

  onGenerateRoute(RouteSettings routeSettings) {}
}
