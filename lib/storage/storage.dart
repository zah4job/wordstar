import 'package:flutter_secure_storage/flutter_secure_storage.dart';

Future<void> saveUsername(String username) async {
  /// Сохранить имя пользователя в хранилище.
  var storage = const FlutterSecureStorage();
  await storage.write(key: 'username', value: username);
}

Future<String> loadUsername() async {
  /// Загрузка имени пользователя из хранилища.
  var storage = const FlutterSecureStorage();
  String username = await storage.read(key: 'username') ?? '';
  return username;
}
