import 'package:flutter/material.dart';
import 'package:wordstar/components/snack_bar.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/pages/solo_game_over/report.dart';

import '../../server/requests.dart';
import '../../storage/storage.dart';
import 'new_word.dart';

const TextStyle titleStyle = TextStyle(
  fontSize: 20,
  fontWeight: FontWeight.bold,
);

class SoloGameOverPage extends StatefulWidget {
  final int gameId;
  final bool win;
  final List<String> playerWords;
  final List<String> botWords;
  const SoloGameOverPage({
    Key? key,
    required this.gameId,
    required this.win,
    required this.playerWords,
    required this.botWords,
  }) : super(key: key);

  @override
  State<SoloGameOverPage> createState() => _SoloGameOverPageState();
}

class _SoloGameOverPageState extends State<SoloGameOverPage> {
  bool isBusy = false;
  Set<String> words = {};
  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Результат игры',
      body: body(),
    );
  }

  Widget body() {
    return Column(
      children: [
        widget.win ? winner() : looser(),
        const NewWord(),
        OutlinedButton(
          onPressed: () => print(123),
          child: const Text('Пожаловаться на выбранные'),
        ),
        playAgainButton(),
        goWelcome(),
      ],
    );
  }

  Widget playAgainButton() {
    return OutlinedButton(
      onPressed: () {
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/rooms',
          (route) => false,
        );
      },
      child: const Text('Играть ещё раз'),
    );
  }

  Widget goWelcome() {
    return OutlinedButton(
        onPressed: () {
          Navigator.pushNamedAndRemoveUntil(
            context,
            '/introduce',
            (route) => false,
          );
        },
        child: const Text('Сменить имя'));
  }

  Widget winner() {
    return Expanded(
      child: Column(
        children: [
          const Text(
            'Ты победитель.',
            textAlign: TextAlign.center,
            style: titleStyle,
          ),
          Expanded(
              child: report(
            widget.botWords,
            widget.playerWords,
            _checkWord,
          )),
        ],
      ),
    );
  }

  Widget looser() {
    return Expanded(
      child: Column(
        children: [
          const Text(
            'Ты проиграл.\r\nНе сдавайся, в следующий раз получится!',
            style: titleStyle,
            textAlign: TextAlign.center,
          ),
          Expanded(
              child: report(
            widget.botWords,
            widget.playerWords,
            _checkWord,
          )),
        ],
      ),
    );
  }

  void _checkWord(bool checked, String word) {
    if (checked) {
      words.add(word);
    } else {
      words.remove(word);
    }
  }

  void _reportWord(String word) async {
    setState(() {
      isBusy = true;
    });
    var userName = await loadUsername();
    var params = {
      'word': word,
      'reporter': userName.isEmpty ? 'debugUser' : userName,
    };
    var response = await serverPost('api/word-report/', params);
    if (response.statusCode == 201) {
      showMessage(
          context, 'Жалоба на слово $word успешно отправлена. Спасибо.');
    } else if (response.statusCode == 208) {
      showMessage(context, 'Слово ${word.toUpperCase()} уже есть среди жалоб');
    } else {
      showMessage(context, 'Ошбка: ${response.statusCode}');
    }

    setState(() {
      isBusy = false;
    });
  }
}
