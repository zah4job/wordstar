import 'package:flutter/material.dart';

typedef OnCheckedCallback = void Function(bool value, String word);

const TextStyle titleStyle = TextStyle(fontSize: 20);

Widget report(
  List<String> botWords,
  List<String> playerWords,
  OnCheckedCallback onChecked,
) {
  return Column(
    children: [
      const SizedBox(height: 15),
      const Text('Были использованы слова:', style: titleStyle),
      const SizedBox(height: 10),
      Expanded(
        child: ListView.builder(
          itemBuilder: (context, index) {
            if (index < botWords.length) {
              return WordItem(
                word: botWords[index],
                onChecked: onChecked,
              );
            }
            return WordItem(
                word: playerWords[index - botWords.length],
                onChecked: onChecked);
          },
          itemCount: botWords.length + playerWords.length,
        ),
      ),
    ],
  );
}

class WordItem extends StatefulWidget {
  final String word;
  final OnCheckedCallback onChecked;

  const WordItem({
    Key? key,
    required this.word,
    required this.onChecked,
  }) : super(key: key);

  @override
  State<WordItem> createState() => _WordItemState();
}

class _WordItemState extends State<WordItem> {
  bool checked = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 0, horizontal: 30),
      child: CheckboxListTile(
        controlAffinity: ListTileControlAffinity.leading,
        value: checked,
        onChanged: (value) => setState(() {
          checked = !checked;
          widget.onChecked(checked, widget.word);
        }),
        title: Text(widget.word),
      ),
    );
  }
}
