import 'package:flutter/material.dart';
import 'package:wordstar/server/requests.dart';
import 'package:wordstar/storage/storage.dart';

class NewWord extends StatefulWidget {
  const NewWord({Key? key}) : super(key: key);

  @override
  State<NewWord> createState() => _NewWordState();
}

class _NewWordState extends State<NewWord> {
  bool isOpen = false;
  bool isBusy = false;
  late TextEditingController controller;

  @override
  void initState() {
    controller = TextEditingController();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30),
          child: Row(
            children: [
              Flexible(
                flex: 1,
                child: TextField(
                  controller: controller,
                  decoration: const InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Введите новое слово',
                  ),
                ),
              ),
              const SizedBox(width: 15),
              Flexible(
                flex: 0,
                child: TextButton(
                  onPressed: isBusy ? null : addWord2Server,
                  child: const Text('Добавить'),
                ),
              )
            ],
          ),
        ),
      ],
    );
  }

  void addWord2Server() async {
    if (controller.text.isEmpty) {
      showMessage('Введите слово, которое хотите добавить');
      return;
    }
    setState(() {
      isBusy = true;
    });
    var playerName = await loadUsername();
    var params = {
      'reporter': playerName,
      'word': controller.text,
    };
    var response = await serverPost('api/new-word/', params);
    var code = response.statusCode;
    if (code != 201) {
      showMessage('Не удалось добавить слово: ошибка сервера $code.');
    } else {
      showMessage(
          'Заявка оставлена. Спасибо, что помогаете делать игру лучше.');
    }
    setState(() {
      isBusy = false;
    });
  }

  void showMessage(String message) {
    SnackBar snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
