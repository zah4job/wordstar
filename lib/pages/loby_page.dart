import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/pages/lobby_page_ws.dart';
import 'package:wordstar/storage/storage.dart';

class LobbyPage extends StatefulWidget {
  final String owner;
  final String visitor;
  final String gameId;

  const LobbyPage({
    Key? key,
    required this.owner,
    required this.gameId,
    required this.visitor,
  }) : super(key: key);

  @override
  State<LobbyPage> createState() => _LobyPageState();
}

class _LobyPageState extends State<LobbyPage> {
  String guest = '???';
  late Future<String> myNameFuture;
  String myName = '';
  bool ownerReady = false;
  bool guestReady = false;
  late TextStyle textStyle;
  late LobbyPageWs lobbyPageWs;
  bool registered = false; // becoming true after one time register
  bool start = false; // мы решили начать игру

  @override
  void initState() {
    guest = widget.visitor;
    lobbyPageWs = LobbyPageWs(
      gameId: widget.gameId,
      playerReady: playerReady,
      playerJoined: playerJoined,
      gameStarted: gameStarted,
    );
    lobbyPageWs.connect();
    myNameFuture = fetchMyName();
    textStyle = const TextStyle(fontSize: 25);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Лобби',
      body: bodyBuilder(),
    );
  }

  @override
  void dispose() {
    if (!start) {
      // при запуске игры - не говорим о ливе
      lobbyPageWs.joined(myName, false);
    }
    lobbyPageWs.dispose();
    super.dispose();
  }

  void playerReady(String playerName, bool isReady) {
    print('player $playerName ready: $isReady');
    setState(() {
      if (playerName == widget.owner) {
        ownerReady = isReady;
      }
      if (playerName == guest) {
        guestReady = isReady;
      }

      goGame();
    });
  }

  void goGame() {
    /// начинаем игру, если все готовы
    if (ownerReady && guestReady) {
      if (myName == widget.owner) {
        lobbyPageWs.startGame(myName, widget.gameId);
      }
    }
  }

  void playerJoined(String playerName, bool isJoined) {
    setState(() {
      if (playerName == myName) {
        // сообщения о собственном подключении можно игнорировать
        return;
      }
      ownerReady = false;
      guestReady = false;
      if (isJoined == false) {
        guest = '???';
      } else {
        guest = playerName;
      }
    });
  }

  void gameStarted() {
    // пришло сообщение о начале игры
    start = true;
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/game',
      (route) => false,
      arguments: {'gameId': int.parse(widget.gameId)},
    );
  }

  Widget bodyBuilder() {
    return FutureBuilder(
        future: myNameFuture,
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return const Center(child: CircularProgressIndicator());
          }
          myName = snapshot.data as String;
          if (!registered) {
            registered = true;
            lobbyPageWs.joined(myName, true);
          }
          return body(myName);
        });
  }

  Widget body(String myName) {
    bool iAmOwner = myName == widget.owner;
    bool iAmGuest = !iAmOwner;
    return Padding(
      padding: const EdgeInsets.only(top: 80, left: 40, right: 20, bottom: 20),
      child: Column(
        children: [
          Row(
            children: [leaveButton()],
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                widget.owner,
                style: textStyle,
              ),
              const Spacer(),
              iAmOwner
                  ? readyButton(ownerReady, iAmOwner, myName)
                  : readyText(ownerReady),
            ],
          ),
          Row(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Text(
                guest,
                style: textStyle,
              ),
              const Spacer(),
              iAmGuest
                  ? readyButton(guestReady, iAmOwner, myName)
                  : readyText(guestReady)
            ],
          ),
          Text(widget.gameId),
        ],
      ),
    );
  }

  Widget readyButton(bool isReady, bool iAmOwner, String myName) {
    if (isReady) {
      return Text(
        'Я готов.',
        style: textStyle,
        textAlign: TextAlign.right,
      );
    }
    return OutlinedButton(
      child: Text(
        'готов победить',
        style: textStyle,
        textAlign: TextAlign.right,
      ),
      onPressed: () {
        lobbyPageWs.ready(myName);
      },
    );
  }

  Widget readyText(bool isReady) {
    return Text(
      isReady ? 'готов' : 'не готов',
      style: textStyle,
    );
  }

  Future<String> fetchMyName() async {
    String myName = await loadUsername();
    return myName;
  }

  Widget leaveButton() {
    return OutlinedButton(
      onPressed: () {
        lobbyPageWs.leave(myName);
        Navigator.pushNamedAndRemoveUntil(
          context,
          '/rooms',
          (route) => false,
        );
      },
      child: const Text('покинуть'),
    );
  }
}
