import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:wordstar/server/requests.dart';

typedef PlayerReadyCallback = void Function(String, bool);
typedef PlayerJoinedCallback = void Function(String, bool);

class LobbyPageWs {
  late String gameId;
  late PlayerReadyCallback playerReady;
  late PlayerJoinedCallback playerJoined;
  late VoidCallback gameStarted;
  WebSocketChannel? channel;

  /// класс для сокета лобби
  LobbyPageWs({
    Key? key,
    required this.gameId,
    required this.playerReady,
    required this.playerJoined,
    required this.gameStarted,
  });

  void connect() {
    String uri = 'ws://$hostDomain/ws/lobby/$gameId/';
    channel = WebSocketChannel.connect(Uri.parse(uri));
    channel?.stream.listen(
      (data) {
        var string = utf8.decode(data);
        var jsonMessage = jsonDecode(string);
        var event = jsonMessage['event'];
        var playerName = jsonMessage['sender'];
        if (event == 'ready') {
          playerReady(playerName, true);
        }
        if (event == 'not ready') {
          playerName(playerName, false);
        }
        if (event == 'joined') {
          playerJoined(playerName, true);
        }
        if (event == 'leave') {
          playerJoined(playerName, false);
        }
        if (event == 'startgame') {
          gameStarted();
        }
      },
      onError: (error) => print(error),
    );
  }

  void dispose() {
    channel?.sink.close();
  }

  void ready(String myName) {
    var params = {
      'sender': myName,
      'event': 'ready',
    };
    var jsonParams = jsonEncode(params);
    channel?.sink.add(jsonParams);
  }

  void leave(String myName) {
    var params = {
      'sender': myName,
      'event': 'leave',
    };
    var jsonParams = jsonEncode(params);
    channel?.sink.add(jsonParams);
  }

  void joined(String playerName, bool isJoined) {
    var params = {
      'sender': playerName,
      'event': isJoined ? 'joined' : 'leave',
    };
    var jsonParams = jsonEncode(params);
    channel?.sink.add(jsonParams);
  }

  void startGame(String playerName, String gameId) {
    var params = {
      'sender': playerName,
      'event': 'startgame',
    };
    var jsonParams = jsonEncode(params);
    channel?.sink.add(jsonParams);
  }
}
