import 'package:flutter/material.dart';
import 'package:wordstar/storage/storage.dart';

import '../components/zscaffold.dart';

class IntroducePage extends StatefulWidget {
  const IntroducePage({Key? key}) : super(key: key);

  @override
  State<IntroducePage> createState() => _IntroducePageState();
}

class _IntroducePageState extends State<IntroducePage> {
  late TextEditingController controller;
  late Future<String> nameFuture;
  String currentName = '';

  @override
  void initState() {
    controller = TextEditingController();
    nameFuture = parseName();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Как тебя зовут?',
      body: body(context, controller),
    );
  }

  Widget bodyBuilder(BuildContext context) {
    return FutureBuilder(
      future: nameFuture,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return body(context, controller);
        }
        if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Future<String> parseName() async {
    String name = await loadUsername();
    setState(() {
      currentName = name;
      controller.text = currentName;
    });
    return name;
  }
}

Widget body(BuildContext context, TextEditingController controller) {
  return Column(
    children: [
      TextField(
        controller: controller,
        decoration: const InputDecoration(
          border: OutlineInputBorder(),
          hintText: 'Введи своё имя',
        ),
      ),
      OutlinedButton(
        child: const Text('OK'),
        onPressed: () {
          saveUsername(controller.text);
          Navigator.pushNamedAndRemoveUntil(
            context,
            '/rooms',
            (route) => false,
            arguments: {'id': '1'},
          );
        },
      ),
    ],
  );
}
