import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/models/loby.dart';

class RoomPage extends StatefulWidget {
  final String roomId;
  const RoomPage({Key? key, required this.roomId}) : super(key: key);

  @override
  State<RoomPage> createState() => _RoomPageState();
}

class _RoomPageState extends State<RoomPage> {
  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Lobby# ${widget.roomId}',
      body: FutureBuilder(
        future: futureLoby(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return roomLayout(snapshot.data as Loby);
          }
          return const CircularProgressIndicator();
        },
      ),
    );
  }

  Future<Loby> futureLoby() async {
    return Loby(
      id: "100",
      owner: "Хост",
      guest: "Гость",
      ownerReady: true,
      guestReady: false,
    );
  }

  Widget roomLayout(Loby loby) {
    String ownerReady = loby.ownerReady ? '+' : '-';
    String guestReady = loby.guestReady ? '+' : '-';
    return Column(
      children: [
        Text('Игрок №1: ${loby.owner}'),
        Text('Игрок №2: ${loby.guest}'),
        Text('Игрок №1 готов: $ownerReady'),
        Text('Игрок №2 готов: $guestReady'),
        OutlinedButton(
          onPressed: () {},
          child: const Text('Я готов'),
        ),
      ],
    );
  }
}
