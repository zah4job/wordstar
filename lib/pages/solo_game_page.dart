import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/server/requests.dart';
import 'package:wordstar/storage/storage.dart';

import '../models/solo_game.dart';

enum GameStage {
  myTurn,
  botTurn,
}

const int turnDuration = 200;

class SoloGamePage extends StatefulWidget {
  const SoloGamePage({
    Key? key,
  }) : super(key: key);

  @override
  State<SoloGamePage> createState() => _SoloGamePageState();
}

class _SoloGamePageState extends State<SoloGamePage> {
  late Future<SoloGame> futureSoloGame;
  String me = '???';
  String botName = 'WordBot';
  late GameStage gameStage;
  late SoloGame game;
  int timer = turnDuration;
  List<String> myWords = [];
  List<String> botWords = [];
  late TextEditingController controller;
  TextStyle style1 = const TextStyle(fontSize: 20);
  bool isBusy = false;
  late Timer timerObject;
  bool iAmWinner = false;

  @override
  void initState() {
    super.initState();
    futureSoloGame = fetchSoloGame();
    gameStage = GameStage.myTurn;
    controller = TextEditingController();
  }

  @override
  void dispose() {
    timerObject.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Игра против Ai',
      body: bodyBuilder(),
    );
  }

  Future<SoloGame> fetchSoloGame() async {
    var name = await loadUsername();
    setState(() {
      me = name;
    });
    var response = await serverPost('api/solo/', {
      'creator': name,
    });
    var jsonResponse = jsonDecode(utf8.decode(response.bodyBytes));
    game = SoloGame.fromJson(jsonResponse);
    timerObject = Timer.periodic(const Duration(seconds: 1), checkGameOver);
    return game;
  }

  Widget bodyBuilder() {
    return FutureBuilder(
        future: futureSoloGame,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return errorBody(snapshot.error.toString());
          }
          if (snapshot.hasData) {
            return body(snapshot.data as SoloGame);
          }
          return const Center(child: CircularProgressIndicator());
        });
  }

  Widget errorBody(String errorText) {
    return Center(child: Text(errorText));
  }

  Widget body(SoloGame soloGame) {
    return Column(children: [
      rowOne(),
      wordRow(),
      wordListRow(),
      controlPanel(),
    ]);
  }

  Widget rowOne() {
    /// Имена игроков, очки и счетчик времени
    return Row(children: [
      Expanded(
        child: nameAndScore(
          me,
          true,
        ),
      ),
      Text(
        '$timer',
        style: const TextStyle(fontSize: 50),
      ),
      Expanded(
        child: nameAndScore(
          botName,
          false,
        ),
      ),
    ]);
  }

  Widget nameAndScore(String name, bool leftAlign) {
    late bool drawBold;
    if (leftAlign) {
      drawBold = gameStage == GameStage.myTurn;
    } else {
      drawBold = gameStage != GameStage.myTurn;
    }

    return Column(
      crossAxisAlignment:
          leftAlign ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
        Text(
          name,
          style: TextStyle(
            fontSize: 20,
            fontWeight: drawBold ? FontWeight.bold : FontWeight.normal,
          ),
        ),
      ],
    );
  }

  Widget wordRow() {
    return Center(
      child: Text(
        game.sourceWord.toUpperCase(),
        style: const TextStyle(fontSize: 23),
      ),
    );
  }

  Widget wordListRow() {
    return Expanded(child: wordsList());
  }

  Widget wordsList() {
    return ListView.builder(
      itemCount: max(myWords.length, botWords.length),
      itemBuilder: (context, index) {
        String myWord = '';
        String opponentWord = '';
        if (myWords.length > index) {
          myWord = myWords[index];
        }
        if (botWords.length > index) {
          opponentWord = botWords[index];
        }
        return Row(children: [
          Expanded(
            child: Text(
              myWord,
              style: style1,
            ),
          ),
          Expanded(
            child: Text(
              opponentWord,
              textAlign: TextAlign.right,
              style: style1,
            ),
          ),
        ]);
      },
    );
  }

  Widget controlPanel() {
    return Opacity(
      opacity: gameStage == GameStage.myTurn ? 1 : 0,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: OutlinedButton(
              onPressed: () => makeTurn(),
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                child: Text('Ответить'),
              ),
            ),
          )
        ],
      ),
    );
  }

  void checkGameOver(Timer sender) {
    setState(() {
      timer = max(timer - 1, 0);
      iAmWinner = gameStage == GameStage.botTurn;
    });

    if (timer == 0) {
      // закончилась игра
      timerObject.cancel();
      Navigator.pushNamedAndRemoveUntil(
        context,
        '/sologameover',
        (route) => false,
        arguments: {
          'gameId': game.pk,
          'win': iAmWinner,
          'playerWords': myWords,
          'botWords': botWords,
        },
      );
    }
  }

  void makeTurn() {
    var word = controller.text;

    if (game.allAnswers.contains(word)) {
      if (!alreadyUsed(word)) {
        setState(() {
          myWords.add(word);
          timer = turnDuration;
          gameStage = GameStage.botTurn;
        });
        botTurn();
      } else {
        showMessage('Слово уже было в игре.');
      }
    } else {
      showMessage('Слова нет в словаре.');
    }
  }

  void botTurn() async {
    var all = game.allAnswers.toSet();
    var answered = (myWords + botWords).toSet();
    var remains = all.difference(answered);
    var letsPlay = remains.length > 10 && botWords.length < 10;

    // if (remains.length > 10) {
    if (letsPlay) {
      var random = Random();
      var randomWord = remains.toList()[random.nextInt(remains.length)];
      int randomDelay = random.nextInt(turnDuration - 2);

      await Future.delayed(
        // Duration(seconds: randomDelay),
        const Duration(seconds: 1),
        () {
          setState(() {
            botWords.add(randomWord);
            timer = turnDuration;
            gameStage = GameStage.myTurn;
            controller.text = '';
          });
        },
      );
    }
  }

  bool alreadyUsed(String word) {
    /// Слово уже исползовалось в этой игре
    return myWords.contains(word) || botWords.contains(word);
  }

  void showMessage(String message) {
    SnackBar snackBar = SnackBar(
      content: Text(message),
      action: SnackBarAction(
        label: 'OK',
        onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
      ),
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
// ScaffoldMessenger.of(context).hideCurrentSnackBar()