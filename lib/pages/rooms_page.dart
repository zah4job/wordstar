import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/models/game.dart';
import 'package:wordstar/models/room.dart';
import 'package:wordstar/server/requests.dart';
import 'package:wordstar/storage/storage.dart';

class RoomsPage extends StatefulWidget {
  const RoomsPage({Key? key}) : super(key: key);

  @override
  State<RoomsPage> createState() => _RoomsPageState();
}

class _RoomsPageState extends State<RoomsPage> {
  List<int> numbers = [1, 2, 3];

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Список игр',
      body: body(),
    );
  }

  Widget body() {
    return FutureBuilder(
      future: futureRooms(),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return Padding(
            padding: const EdgeInsets.all(20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                soloButton(),
                refreshButton(),
                newGameButton(),
                const Text('Выбрать противника:'),
                Expanded(child: roomListView(snapshot.data as List<GameModel>)),
              ],
            ),
          );
        }
        if (snapshot.hasError) {
          return Center(child: Text('${snapshot.error}'));
        }
        return const CircularProgressIndicator();
      },
    );
  }

  Widget roomListView(List<GameModel> rooms) {
    int count = rooms.length;

    if (count == 0) {
      return noGames();
    }

    return ListView.builder(
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        var room = rooms[index];
        return OutlinedButton(
          child: Align(
            alignment: Alignment.centerLeft,
            child: Text(
              room.owner,
              textAlign: TextAlign.left,
            ),
          ),
          onPressed: () {
            loadUsername().then((myName) => {
                  Navigator.pushNamedAndRemoveUntil(
                    context,
                    '/lobby',
                    (route) => false,
                    arguments: {
                      'gameId': room.pk.toString(),
                      'owner': room.owner,
                      'visitor': myName,
                    },
                  )
                });
          },
        );
      },
    );
  }

  Widget noGames() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: const [
        Text(
          '...Сейчас нет открытых игр...',
          style: TextStyle(color: Colors.grey),
        ),
      ],
    );
  }

  Widget soloButton() {
    return OutlinedButton(
      child: const Text('Игра c Ai'),
      onPressed: () => Navigator.pushNamedAndRemoveUntil(
          context, '/sologame', (route) => false),
    );
  }

  Widget refreshButton() {
    return OutlinedButton(
      child: const Text('Обновить список'),
      onPressed: () => Navigator.pushNamedAndRemoveUntil(
          context, '/rooms', (route) => false),
    );
  }

  Widget newGameButton() {
    return OutlinedButton(
      child: const Text('Создать новую игру'),
      onPressed: () {
        loadUsername().then((myName) {
          serverPost(
            'api/game/new-game/',
            {
              'owner': myName,
            },
          ).then((response) {
            print(response.statusCode);
            String gameId = utf8.decode(response.bodyBytes).replaceAll('"', '');
            Navigator.pushNamedAndRemoveUntil(
              context,
              '/lobby',
              (route) => false,
              arguments: {
                'gameId': gameId,
                'owner': myName,
                'visitor': '???',
              },
            );
          });
        });
      },
    );
  }

  Future<List<GameModel>> futureRooms() async {
    var response = await serverGet('api/game/');
    if (response.statusCode != 200) {
      throw Exception('Ошибка загрузки: ${response.statusCode}');
    }
    var body_text = utf8.decode(response.bodyBytes);
    var list = jsonDecode(body_text) as List;
    List<GameModel> games =
        list.map((item) => GameModel.fromJson(item)).toList();

    return games;
  }
}
