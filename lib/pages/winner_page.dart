import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';

class WinnerPage extends StatelessWidget {
  final String winner;
  const WinnerPage({Key? key, required this.winner}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Победитель тут',
      body: body(context),
    );
  }

  Widget body(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            Text(
              'Победитель',
              style: TextStyle(fontSize: 18),
            )
          ],
        ),
        Text(
          winner,
          style: const TextStyle(fontSize: 25),
        ),
        OutlinedButton(
          child: const Text('Играть ещё раз'),
          onPressed: () {
            Navigator.pushNamedAndRemoveUntil(
              context,
              '/introduce',
              (route) => false,
            );
          },
        )
      ],
    );
  }
}
