import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:wordstar/components/zscaffold.dart';
import 'package:wordstar/models/game.dart';
import 'package:wordstar/pages/game_page_ws.dart';
import 'package:wordstar/storage/storage.dart';
import 'package:wordstar/server/requests.dart';

class GamePage extends StatefulWidget {
  final int gameId;
  const GamePage({
    Key? key,
    required this.gameId,
  }) : super(key: key);

  @override
  State<GamePage> createState() => _GamePageState();
}

enum GameStage {
  myTurn,
  opponentTurn,
}

class _GamePageState extends State<GamePage> {
  String me = 'Буквожор';
  String opponent = 'Говноша';
  String owner = '?';
  final int timerCapacity = 20;
  late int timer;
  int myPoints = 0;
  int opponentPoints = 12;
  String sourceWord = '???';
  GameStage gameStage = GameStage.myTurn;
  late GameSocket ws;
  late Future<GameModel> futureGame;
  late Timer timerObject;

  late TextEditingController controller;
  List<String> myWords = [];
  List<String> opponentWords = [];
  TextStyle style1 = const TextStyle(fontSize: 20);
  bool isBusy = false;

  @override
  void initState() {
    timer = timerCapacity;
    controller = TextEditingController();
    futureGame = fetchGame(widget.gameId);
    timerObject = Timer.periodic(const Duration(seconds: 1), (timer) {
      onTimer();
    });
    super.initState();
  }

  @override
  void dispose() {
    ws.dispose();
    if (timerObject.isActive) {
      timerObject.cancel();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return zScaffold(
      context: context,
      title: 'Сракжение',
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: FutureBuilder(
          future: futureGame,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return body(snapshot.data as GameModel);
            }
            if (snapshot.hasError) {
              return Center(child: Text('${snapshot.error}'));
            }
            return const Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }

  Widget body(GameModel gameModel) {
    return Column(children: [
      rowOne(),
      wordRow(),
      playerWords(),
      controlPanel(),
    ]);
  }

  Future<GameModel> fetchGame(int gameId) async {
    String url = 'api/game/$gameId/';
    final response = await serverGet(url);
    if (response.statusCode != 200) {
      SnackBar snackBar = SnackBar(content: Text('${response.statusCode}'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      throw Exception(response.body);
    }

    // выяснение имени оппонента
    var myName = await loadUsername();
    me = myName;
    var gameModel =
        GameModel.fromJson(jsonDecode(utf8.decode(response.bodyBytes)));
    if (gameModel.owner == me) {
      opponent = gameModel.visitor;
    } else if (gameModel.visitor == me) {
      opponent = gameModel.owner;
    } else {
      throw Exception('Тебя тут не любят.');
    }
    // ход игры
    if (gameModel.nextTurn == me) {
      gameStage = GameStage.myTurn;
      print('game stage by model: ${gameStage}');
    } else {
      gameStage = GameStage.opponentTurn;
      print('game stage by model: ${gameStage}');
    }
    // запоминание данных
    sourceWord = gameModel.sourceWord;
    owner = gameModel.owner;

    // настройка сокета
    ws = GameSocket(
      gameId: gameId,
      myName: me,
      oppoName: opponent,
      onGameOver: onGameOver,
      myTurn: (String newWord) {
        setState(() {
          opponentWords.add(newWord);
          gameStage = GameStage.myTurn;
          controller.text = '';
          timer = timerCapacity;
        });
      },
      oppoTurn: (String newWord) {
        setState(() {
          myWords.add(newWord);
          gameStage = GameStage.opponentTurn;
          timer = timerCapacity;
        });
      },
    );
    ws.connect();

    return gameModel;
  }

  Widget rowOne() {
    /// Имена игроков, очки и счетчик времени
    return Row(children: [
      Expanded(
        child: nameAndScore(
          me,
          myPoints,
          true,
        ),
      ),
      Text(
        '$timer',
        style: const TextStyle(fontSize: 50),
      ),
      Expanded(
        child: nameAndScore(
          opponent,
          opponentPoints,
          false,
        ),
      ),
    ]);
  }

  Widget nameAndScore(String name, int score, bool leftAlign) {
    late bool drawBold;
    if (leftAlign) {
      drawBold = gameStage == GameStage.myTurn;
    } else {
      drawBold = gameStage != GameStage.myTurn;
    }

    return Column(
      crossAxisAlignment:
          leftAlign ? CrossAxisAlignment.start : CrossAxisAlignment.end,
      children: [
        Text(
          name,
          style: TextStyle(
            fontSize: 20,
            fontWeight: drawBold ? FontWeight.bold : FontWeight.normal,
          ),
        ),
        // Text(
        //   '$score',
        //   textAlign: leftAlign ? TextAlign.left : TextAlign.right,
        //   style: const TextStyle(fontSize: 20),
        // ),
      ],
    );
  }

  Widget wordRow() {
    return Center(
      child: Text(
        sourceWord.toUpperCase(),
        style: const TextStyle(fontSize: 23),
      ),
    );
  }

  Widget playerWords() {
    return Expanded(child: wordsList());
  }

  Widget controlPanel() {
    return Opacity(
      opacity: gameStage == GameStage.myTurn ? 1 : 0,
      child: Row(
        children: [
          Expanded(
            child: TextField(
              controller: controller,
              decoration: const InputDecoration(
                border: OutlineInputBorder(),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            child: OutlinedButton(
              onPressed: isBusy
                  ? null
                  : () {
                      sendWord2Server();
                    },
              child: const Padding(
                padding: EdgeInsets.symmetric(vertical: 20.0),
                child: Text('Ответить'),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget wordsList() {
    return ListView.builder(
      itemCount: max(myWords.length, opponentWords.length),
      itemBuilder: (context, index) {
        String myWord = '';
        String opponentWord = '';
        if (myWords.length > index) {
          myWord = myWords[index];
        }
        if (opponentWords.length > index) {
          opponentWord = opponentWords[index];
        }
        return Row(children: [
          Expanded(
            child: Text(
              myWord,
              style: style1,
            ),
          ),
          Expanded(
            child: Text(
              opponentWord,
              textAlign: TextAlign.right,
              style: style1,
            ),
          ),
        ]);
      },
    );
  }

  void sendWord2Server() async {
    setState(() {
      isBusy = true;
    });
    // отправка ответа на сервер
    var response = await serverPost('api/game/${widget.gameId}/make-turn/', {
      'word': controller.text,
      'author': me,
    });
    setState(() {
      isBusy = false;
    });
    if (response.statusCode != 201) {
      late String message;
      var reason = utf8.decode(response.bodyBytes);
      if (reason == '{"word":["такого слова нет"]}') {
        message = 'Слова нет в словаре';
      } else if (reason == '["слово уже есть"]') {
        message = 'Слово уже использовалось';
      } else {
        message = 'Ошибка: ${response.statusCode}';
        message += '>> ${utf8.decode(response.bodyBytes)}';
      }

      SnackBar snackBar = SnackBar(content: Text(message));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      // слово принято на сервере - добавляем его в игру
      ws.makeTurn(me, controller.text);
    }
  }

  void onTimer() {
    setState(() {
      timer -= 1;
      if (timer <= 0) {
        timerObject.cancel();
        if (me == owner) {
          var winner = gameStage == GameStage.myTurn ? opponent : me;
          ws.gameOver(me, winner);
        }
      }
    });
  }

  void onGameOver(String winner) {
    /// сокет сказал, что игра окончена
    Navigator.pushNamedAndRemoveUntil(
      context,
      '/winner',
      (route) => false,
      arguments: {'winner': winner},
    );
  }
}
