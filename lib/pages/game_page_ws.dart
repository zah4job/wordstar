import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:web_socket_channel/web_socket_channel.dart';
import 'package:wordstar/server/requests.dart';

typedef StringToVoidFunc = void Function(String);
typedef GameOverCallback = void Function(String, String);

class GameSocket {
  WebSocketChannel? channel;
  Stream? stream;
  late String myName;
  late String oppoName;
  late StringToVoidFunc myTurn;
  late StringToVoidFunc oppoTurn;
  late StringToVoidFunc onGameOver;
  late int gameId;

  GameSocket({
    Key? key,
    required this.gameId,
    required this.myName,
    required this.oppoName,
    required this.myTurn,
    required this.oppoTurn,
    required this.onGameOver,
  });

  /// websocket для игры

  void connect() {
    String uri = 'ws://$hostDomain/ws/game/$gameId/';
    channel = WebSocketChannel.connect(Uri.parse(uri));
    channel?.stream.listen(
      (data) {
        print(data);
        var message = jsonDecode(utf8.decode(data));
        var sender = message['sender'];
        var event = message['event'];

        if (event == 'gameover') {
          var winner = message['winner'];
          onGameOver(winner);
        }
        if (event == 'turn') {
          var newWord = message['word'];
          if (sender == myName) {
            oppoTurn(newWord);
          } else if (sender == oppoName) {
            myTurn(newWord);
          }
        }
      },
      onError: (error) => print(error),
    );
    if (channel?.stream != null) {
      stream = channel?.stream as Stream;
    }

    // StreamBuilder(
    //   stream: channel?.stream,
    //   builder: (context, snapshot) {
    //     return Text(snapshot.hasData ? '${snapshot.data}' : '');
    //   },
    // );
  }

  void makeTurn(String userName, String newWord) {
    var params = {
      'sender': userName,
      'event': 'turn',
      'word': newWord,
    };
    var encoded = jsonEncode(params);
    channel?.sink.add(encoded);
  }

  void gameOver(String sender, String winner) {
    /// игра окончена
    var params = {
      'sender': sender,
      'event': 'gameover',
      'winner': winner,
    };
    var jsonParams = jsonEncode(params);
    channel?.sink.add(jsonParams);
  }

  void dispose() {
    channel?.sink.close();
  }
}
