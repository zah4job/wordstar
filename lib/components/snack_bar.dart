import 'package:flutter/material.dart';

void showMessage(BuildContext context, String message) {
  SnackBar snackBar = SnackBar(
    content: Text(message),
    action: SnackBarAction(
      label: 'OK',
      onPressed: () => ScaffoldMessenger.of(context).hideCurrentSnackBar(),
    ),
  );
  ScaffoldMessenger.of(context).showSnackBar(snackBar);
}
