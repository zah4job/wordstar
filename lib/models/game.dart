class GameModel {
  GameModel({
    required this.pk,
    required this.owner,
    required this.visitor,
    required this.isStarted,
    required this.isFinished,
    required this.createdAt,
    required this.sourceWord,
    required this.nextTurn,
  });
  late final int pk;
  late final String owner;
  late final String visitor;
  late final bool isStarted;
  late final bool isFinished;
  late final String createdAt;
  late final String sourceWord;
  late final String nextTurn;

  GameModel.fromJson(Map<String, dynamic> json) {
    pk = json['pk'];
    owner = json['owner'];
    visitor = json['visitor'];
    isStarted = json['is_started'];
    isFinished = json['is_finished'];
    createdAt = json['created_at'];
    sourceWord = json['source_word'];
    nextTurn = json['next_turn'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pk'] = pk;
    _data['owner'] = owner;
    _data['visitor'] = visitor;
    _data['is_started'] = isStarted;
    _data['is_finished'] = isFinished;
    _data['created_at'] = createdAt;
    _data['source_word'] = sourceWord;
    return _data;
  }
}
