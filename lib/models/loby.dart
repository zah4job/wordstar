class Loby {
  Loby({
    required this.id,
    required this.owner,
    required this.guest,
    required this.ownerReady,
    required this.guestReady,
  });
  late final String id;
  late final String owner;
  late final String guest;
  late final bool ownerReady;
  late final bool guestReady;
  
  Loby.fromJson(Map<String, dynamic> json){
    id = json['id'];
    owner = json['owner'];
    guest = json['guest'];
    ownerReady = json['owner_ready'];
    guestReady = json['guest_ready'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['id'] = id;
    _data['owner'] = owner;
    _data['guest'] = guest;
    _data['owner_ready'] = ownerReady;
    _data['guest_ready'] = guestReady;
    return _data;
  }
}