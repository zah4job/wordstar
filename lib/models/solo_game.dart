class SoloGame {
  SoloGame({
    required this.pk,
    required this.creator,
    required this.isStarted,
    required this.isFinished,
    required this.createdAt,
    required this.sourceWord,
    required this.isWin,
    required this.allAnswersCount,
    required this.allAnswers,
  });
  late final int pk;
  late final String creator;
  late final bool isStarted;
  late final bool isFinished;
  late final String createdAt;
  late final String sourceWord;
  late final bool? isWin;
  late final int allAnswersCount;
  late final List<String> allAnswers;

  SoloGame.fromJson(Map<String, dynamic> json) {
    pk = json['pk'];
    creator = json['creator'];
    isStarted = json['is_started'];
    isFinished = json['is_finished'];
    createdAt = json['created_at'];
    sourceWord = json['source_word'];
    isWin = json['is_win'];
    allAnswersCount = json['all_answers_count'];
    allAnswers = List.castFrom<dynamic, String>(json['all_answers']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['pk'] = pk;
    _data['creator'] = creator;
    _data['is_started'] = isStarted;
    _data['is_finished'] = isFinished;
    _data['created_at'] = createdAt;
    _data['source_word'] = sourceWord;
    _data['is_win'] = isWin;
    _data['all_answers_count'] = allAnswersCount;
    _data['all_answers'] = allAnswers;
    return _data;
  }
}
