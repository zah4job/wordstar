import 'dart:convert';

import 'package:http/http.dart' as http;

// const String host = 'http://4ff4-46-242-22-9.ngrok.io/';
const String hostDomain = '89.223.124.15:8001';
// const String hostDomain = '192.168.88.247:8000';
const String host = 'http://$hostDomain/';

const String username = 'android';
const String password = 'Ob4vGi&boTE@ii^sVDBt1a87f';

Future<http.Response> serverGet(String url) async {
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await http.get(
    Uri.parse(host + url),
    headers: <String, String>{'authorization': basicAuth},
  );

  return response;
}

Future<http.Response> serverPost(String url, Map<String, String> params) async {
  String basicAuth =
      'Basic ' + base64Encode(utf8.encode('$username:$password'));
  print(basicAuth);

  final response = await http.post(
    Uri.parse(host + url),
    headers: <String, String>{
      'authorization': basicAuth,
      'Content-Type': 'application/json; charset=UTF-8'
    },
    body: jsonEncode(params),
  );

  return response;
}
